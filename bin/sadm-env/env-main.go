// Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
// See LICENSE file.

package main

import "gitlab.com/amidamaru/sadm/lib/go/cmd/env"

func main() {
	env.Main()
}
