PREFIX ?= /usr/local
DESTDIR ?=

.PHONY: all
all: build

.PHONY: build
build:
	@mkdir -vp ./build/bin
	go build -o ./build/bin/sadm-env ./bin/sadm-env

.PHONY: gofmt
gofmt:
	@gofmt -w -l -s .

.PHONY: check
check:
	go vet ./lib/go/...
	go test ./lib/go/...

.PHONY: clean
clean:
	@rm -rvf ./build
