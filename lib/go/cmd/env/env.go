// Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
// See LICENSE file.

package env

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"strings"

	"gitlab.com/amidamaru/sadm/lib/go/internal/flags"
	"gitlab.com/amidamaru/sadm/lib/go/internal/log"
	_ "gitlab.com/amidamaru/sadm/lib/go/internal/profile/load"
	"gitlab.com/amidamaru/sadm/lib/go/profile"
)

var profName string
var debug string

var sprintf = fmt.Sprintf

func init() {
	profName = os.Getenv("SADM_PROFILE")
	if profName == "" {
		profName = "default"
	}
	flag.StringVar(&profName, "profile", profName, "load env for profile `name`")
}

func Main() {
	flags.Parse()
	log.D("env main")
	if flags.Debug {
		debug = "true"
	} else {
		debug = "false"
	}
	fmt.Print(New())
}

type Env struct {
	data map[string]string
}

func New() *Env {
	return &Env{map[string]string{
		"HOME":        flags.HomeDir,
		"PROFILE":     profName,
		"PROFILE_ALL": strings.Join(profile.List(), " "),
		"OS":          runtime.GOOS,
		"ARCH":        runtime.GOARCH,
		"DEBUG":       debug,
	}}
}

func (e *Env) String() string {
	s := ""
	for k, v := range e.data {
		s = sprintf("%sSADM_%s=\"%s\"\n", s, k, v)
	}
	return s
}

func (e *Env) Get(name string) string {
	if v, ok := e.data[name]; ok {
		return v
	}
	return ""
}
