// Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
// See LICENSE file.

package env

import (
	"path/filepath"
	"runtime"
	"testing"

	"gitlab.com/amidamaru/sadm/lib/go/internal/_t/check"
)

var fromslash = filepath.FromSlash

func TestEnv(t *testing.T) {
	e := New()
	if check.NotEqual(t, e.Get("HOME"), fromslash("/opt/sadm"), "HOME") {
		t.Fail()
	}
	if check.NotEqual(t, e.Get("PROFILE"), "default", "PROFILE") {
		t.Fail()
	}
	if check.NotEqual(t, e.Get("OS"), runtime.GOOS, "OS") {
		t.Fail()
	}
	if check.NotEqual(t, e.Get("ARCH"), runtime.GOARCH, "ARCH") {
		t.Fail()
	}
	if check.NotEqual(t, e.Get("DEBUG"), debug, "DEBUG") {
		t.Fail()
	}
}
