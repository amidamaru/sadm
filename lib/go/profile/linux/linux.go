// Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
// See LICENSE file.

package linux

import (
	//~ "gitlab.com/amidamaru/sadm/lib/go/internal/log"
	"gitlab.com/amidamaru/sadm/lib/go/profile"
)

type Profile struct{}

func init() {
	profile.Register(&Profile{})
}

func (p *Profile) Name() string {
	return "linux"
}
