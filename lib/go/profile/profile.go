// Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
// See LICENSE file.

package profile

import (
	"gitlab.com/amidamaru/sadm/lib/go/internal/log"
)

var m *ProfileManager

type Profile interface {
	Name() string
}

type ProfileManager struct {
	reg map[string]Profile
}

func init() {
	m = &ProfileManager{reg: make(map[string]Profile)}
}

func Register(p Profile) {
	n := p.Name()
	if _, ok := m.reg[n]; ok {
		log.Panic("profile %s already registered", n)
	}
	m.reg[n] = p
}

func List() []string {
	l := make([]string, 0)
	for k := range m.reg {
		l = append(l, k)
	}
	return l
}
