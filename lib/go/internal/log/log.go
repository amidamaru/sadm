// Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
// See LICENSE file.

package log

import (
	xlog "log"
	"os"
)

type printFunc func(fmt string, args ...interface{})
type panicFunc func(fmt string, args ...interface{})

var D printFunc
var E printFunc
var P printFunc
var Panic panicFunc

var lerr *xlog.Logger
var lout *xlog.Logger
var flags = xlog.Ldate | xlog.Ltime

func init() {
	lerr = xlog.New(os.Stderr, "", flags)
	lout = xlog.New(os.Stdout, "", flags)
	D = disable
	E = lerr.Printf
	P = lout.Printf
	Panic = lerr.Panicf
}

func disable(_ string, _ ...interface{}) {
}

func SetDebug(enable bool) {
	if enable {
		D = lerr.Printf
	}
}
