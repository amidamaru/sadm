// Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
// See LICENSE file.

package flags

import (
	"flag"
	"os"
	"path/filepath"

	"gitlab.com/amidamaru/sadm/lib/go/internal/log"
)

var Debug bool
var HomeDir string

func init() {
	defHome := os.Getenv("SADM_HOME")
	if defHome == "" {
		defHome = filepath.FromSlash("/opt/sadm")
	}
	flag.BoolVar(&Debug, "debug", false, "enable debug")
	flag.StringVar(&HomeDir, "homedir", defHome, "home `directory`")
}

func Parse() {
	flag.Parse()
	log.SetDebug(Debug)
}
